using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EvenFibonacciNumbers
{
    public class Program
    {
        public void Main(string[] args)
        {
            var fibonacciNumbers = GenerateFibonacciNumbers(4000000);
            var sum = fibonacciNumbers.Where(f => f % 2 == 0).Sum();
            
            Console.WriteLine("The sum of all even fibonacci numbers less than 4,000,000 is " + sum);
            Console.ReadLine();
        }
        
        public IList<int> GenerateFibonacciNumbers(int upToValue)
        {
            var result = new List<int>
            {
                0,
                1
            };
            
            int nextNumber;
            int counter = 2;
            while(true)
            {
                nextNumber = result[counter - 2] + result[counter - 1];
                
                if (nextNumber > upToValue) break;
                
                result.Add(nextNumber);
                counter++;
            } 
            
            return result;
        }
    }
}
